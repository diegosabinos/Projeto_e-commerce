<?php

use \LS\PageAdmin;
use \LS\Model\User;
use \LS\Model\Category;
use \LS\Model\Product;

$app->get('/admin/categories', function () {
    User::verifyLogin();

    $categories = Category::listAll();
    $page = new PageAdmin();
    $page->setTpl("categories", array("categories" => $categories));
});

$app->get('/admin/categories/create', function () {
    User::verifyLogin();
    $page = new PageAdmin();
    $page->setTpl("categories-create");
});

$app->post('/admin/categories/create', function () {
    User::verifyLogin();
    $category = new Category();
    $category->setData($_POST);
    $category->save();
    header("Location: /admin/categories");
    exit;
});

$app->get('/admin/categories/:idcategory/delete', function ($idcategory) {
    User::verifyLogin();
    $categoria = new Category();
    $categoria->get((int)$idcategory);
    $categoria->delete();
    header("Location: /admin/categories");
    exit;
});


$app->get('/admin/categories/:idcategory', function ($idcategory) {
    User::verifyLogin();
    $categoria = new Category();
    $categoria->get((int)$idcategory);
    $page = new PageAdmin();
    $page->setTpl("categories-update", ['category' => $categoria->getValues()]);

});

$app->post('/admin/categories/:idcategory', function ($idcategory) {
    User::verifyLogin();
    $categoria = new Category();
    $categoria->get((int)$idcategory);
    $categoria->setData($_POST);
    $categoria->save();

    header("Location: /admin/categories");
    exit;
});


$app->get('/admin/categories/:idcategory/products', function ($idcategory) {
    User::verifyLogin();
    $category = new Category();
    $category->get((int)$idcategory);
    $page = new PageAdmin();

    $page->setTpl("categories-products", [
        'category' => $category->getValues(),
        'productsRelated' => Category::getProducts($idcategory, true),
        'productsNotRelated' => Category::getProducts($idcategory, false)
    ]);

});


$app->get('/admin/categories/:idcategory/products/:idproduct/add', function ($idcategory, $idproduct) {
    User::verifyLogin();
    $category = new Category();
    $category->get((int)$idcategory);
    $produto = new Product();
    $produto->get((int)$idproduct);
    $category->addProduto($produto);
    header("Location: /admin/categories/" . $idcategory . "/products");
    exit;
});

$app->get('/admin/categories/:idcategory/products/:idproduct/remove', function ($idcategory, $idproduct) {
    User::verifyLogin();
    $category = new Category();
    $category->get((int)$idcategory);
    $product = new Product();
    $product->get((int)$idproduct);
    $category->removeProduto($product);
    header("Location: /admin/categories/" . $idcategory . "/products");
    exit;
});