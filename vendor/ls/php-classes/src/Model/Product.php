<?php

namespace LS\Model;

use \LS\Model;
use \LS\DB\Sql;

class Product extends Model
{
    protected $fields = [
        "idproduct", "desproduct", "vlprice", "vlwidth", "vlheight", "vllength", "vlweight", "desurl", "dtregister", "desphoto"
    ];

    public static function listAll()
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_products ORDER BY desproduct");
    }

    public static function checkList($list)
    {
        foreach ($list as &$row) {
            $p = new Product();
            $p->setData($row);
            $row = $p->getValues();
        }
        return $list;
    }

    public static function formatarPreco($preco)
    {

    }

    public function save()
    {
        $sql = new Sql();
        $results = $sql->select("CALL sp_products_save(:IDPRODUCT, :DESPRODUCT, :VLPRICE, :VLWIDTH, :VLHEIGHT, :VLLENGTH, :VLWEIGHT, :DESURL)", array(
            ":IDPRODUCT" => $this->getidproduct(),
            ":DESPRODUCT" => $this->getdesproduct(),
            ":VLPRICE" => $this->getvlprice(),
            ":VLWIDTH" => $this->getvlwidth(),
            ":VLHEIGHT" => $this->getvlheight(),
            ":VLLENGTH" => $this->getvllength(),
            ":VLWEIGHT" => $this->getvlweight(),
            ":DESURL" => $this->getdesurl()
        ));
        $this->setData($results[0]);
    }

    public function get($idproduto)
    {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM tb_products WHERE idproduct = :IDPRODUCT", array(
            ':IDPRODUCT' => $idproduto
        ));
        $this->setData($results[0]);
    }

    public function delete()
    {
        $sql = new Sql();
        $sql->query("DELETE FROM tb_products WHERE idproduct = :IDPRODUCT", array(
            ":IDPRODUCT" => $this->getidproduct()
        ));
    }

    public function checkPhoto()
    {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR .
            "res" . DIRECTORY_SEPARATOR .
            "site" . DIRECTORY_SEPARATOR .
            "img" . DIRECTORY_SEPARATOR .
            "products" . DIRECTORY_SEPARATOR . $this->getidproduct() . ".jpg")) {
            $url = "/res/site/img/products/" . $this->getidproduct() . ".jpg";
        } else {
            $url = "/res/site/img/product.jpg";
        }

        return $this->setdesphoto($url);
    }

    public function getValues()
    {
        $this->checkPhoto();

        $values = parent::getValues();

        return $values;
    }

    public function setPhoto($file)
    {
        $extensao = explode('.', $file['name']);
        $extensao = end($extensao);
        switch ($extensao) {
            case "jpg":
            case "jpeg":
                $image = imagecreatefromjpeg($file["tmp_name"]);
                break;
            case "gif":
                $image = imagecreatefromgif($file["tmp_name"]);
                break;
            case "png":
                $image = imagecreatefrompng($file["tmp_name"]);
                break;
        }
        $destino = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "res" . DIRECTORY_SEPARATOR . "site" . DIRECTORY_SEPARATOR . "img" . DIRECTORY_SEPARATOR . "products" . DIRECTORY_SEPARATOR . $this->getidproduct() . ".jpg";
        imagejpeg($image, $destino);
        imagedestroy($image);
        $this->checkPhoto();
    }

    public function getFromURL($url)
    {
        $sql = new Sql();
        $result = $sql->select("SELECT * FROM tb_products WHERE desurl = :URL LIMIT 1", [
            ':URL' => $url
        ]);
        $this->setData($result[0]);
    }

    public function getCategories()
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_categories c INNER JOIN tb_productscategories pc ON c.idcategory = pc.idcategory
                                        WHERE pc.idproduct = :ID", [
            ':ID' => $this->getidproduct()
        ]);
    }

}
