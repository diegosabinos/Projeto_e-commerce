<?php
/**
 * Created by PhpStorm.
 * User: Diego Sabino
 * Date: 09/02/2018
 * Time: 17:15
 */

namespace LS\Model;

use \LS\Model;
use \LS\DB\Sql;

class Category extends Model
{
    protected $fields = [
        "idcategory", "descategory", "dtregister"
    ];

    public static function listAll()
    {
        $sql = new Sql();
        return $sql->select("SELECT * FROM tb_categories ORDER BY descategory");
    }

    public function save()
    {
        $sql = new Sql();
        $results = $sql->select("CALL sp_categories_save(:IDCATEGORY, :DESCATEGORY)", array(
            ":IDCATEGORY" => $this->getidcategory(),
            ":DESCATEGORY" => $this->getdescategory()
        ));
        $this->setData($results[0]);
        Category::updateFile();
    }

    public function get($idcategoria)
    {
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM tb_categories WHERE idcategory = :IDCATEGORIA", array(
            ':IDCATEGORIA' => $idcategoria
        ));
        $this->setData($results[0]);
    }

    public function delete()
    {
        $sql = new Sql();
        $sql->query("DELETE FROM tb_categories WHERE idcategory = :IDCATEGORIA", array(
            ':IDCATEGORIA' => $this->getidcategory()
        ));
        Category::updateFile();
    }

    public static function updateFile()
    {
        $categorias = Category::listAll();
        $html = [];
        foreach ($categorias as $linha) {
            array_push($html, '<li><a href="/categories/' . $linha['idcategory'] . '">' . $linha['descategory'] . '</a></li>');
        }
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "categorias-menu.html", implode('', $html));
    }

    public function getProducts($idcategoria, $relacionado = true)
    {
        $sql = new Sql();
        if ($relacionado === true) {
            return $sql->select("SELECT *
                                            FROM tb_products
                                           WHERE idproduct IN (
                                              SELECT a.idproduct
                                              FROM tb_products a
                                                INNER JOIN tb_productscategories b ON a.idproduct = b.idproduct
                                              WHERE idcategory = :IDCATEGOTY)", [':IDCATEGOTY' => $idcategoria
            ]);
        } else {
            return $sql->select("SELECT *
                                            FROM tb_products
                                           WHERE idproduct NOT IN (
                                              SELECT a.idproduct
                                              FROM tb_products a
                                                INNER JOIN tb_productscategories b ON a.idproduct = b.idproduct
                                              WHERE idcategory = :IDCATEGOTY)", [':IDCATEGOTY' => $idcategoria
            ]);
        }

    }

    public function addProduto(Product $produto)
    {
        $sql = new Sql();
        $sql->query('INSERT INTO tb_productscategories (idcategory, idproduct) VALUES (:CATEGORIA, :PRODUTO)', [
            ':CATEGORIA' => $this->getidcategory(),
            ':PRODUTO' => $produto->getidproduct()
        ]);
    }

    public function removeProduto(Product $produto)
    {
        $sql = new Sql();
        $sql->query('DELETE FROM tb_productscategories WHERE idcategory = :CATEGORIA AND idproduct = :PRODUTO', [
            ':CATEGORIA' => $this->getidcategory(),
            ':PRODUTO' => $produto->getidproduct()
        ]);
    }

    public function getPaginaProduto($pagina = 1, $itemPorPagina = 3)
    {
        $start = ($pagina - 1) * $itemPorPagina;
        $sql = new Sql();
        $result = $sql->select("SELECT SQL_CALC_FOUND_ROWS * 
                                  FROM tb_products p
                            INNER JOIN tb_productscategories pc on p.idproduct = pc.idproduct
                            INNER JOIN tb_categories c ON pc.idcategory = c.idcategory
                                 WHERE c.idcategory = :IDCATEGORY
                                 LIMIT $start, $itemPorPagina;", [
            ":IDCATEGORY" => $this->getidcategory()
        ]);
        $resultTotal = $sql->select("SELECT FOUND_ROWS() AS nrtotal");

        return [
            'data' => Product::checkList($result),
            'total' => $resultTotal[0]["nrtotal"],
            'pages' => ceil($resultTotal[0]["nrtotal"] / $itemPorPagina)
        ];
    }
}