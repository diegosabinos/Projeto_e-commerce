<?php
/**
 * Created by PhpStorm.
 * User: Diego Sabino
 * Date: 08/02/2018
 * Time: 01:43
 */

namespace LS;

use Rain\Tpl;

class Mailer
{

<<<<<<< HEAD
    const USERNAME = "lssuportephp@gmail.com";
    const PASSWORD = "<?senha?>";
=======
    const USERNAME = "suporte@lssuporte.com";
    const PASSWORD = "*************";
>>>>>>> c4ef1c8363ad4e6cbee029661424666a777d41e7
    const NAME_FROM = "Suporte LS";
    private $mail;

    public function __construct($endereco, $nomeDestino, $assunto, $templateName, $data = array())
    {
        $config = array(
            "base_url" => null,
            "tpl_dir" => $_SERVER['DOCUMENT_ROOT'] . "/views/email/",
            "cache_dir" => $_SERVER['DOCUMENT_ROOT'] . "/views-cache/",
            "debug" => false
        );

        Tpl::configure($config);

        $tpl = new Tpl();

        foreach ($data as $chave => $valor) {
            $tpl->assign($chave, $valor);
        }

        $html = $tpl->draw($templateName, true);

        $this->mail = new \PHPMailer();

        $this->mail->isSMTP();

        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        $this->mail->SMTPDebug = 0;

        $this->mail->Host = 'smtp.gmail.com';

        $this->mail->Port = 587;

        $this->mail->SMTPSecure = 'tls';

        $this->mail->SMTPAuth = true;

        $this->mail->Username = Mailer::USERNAME;

        $this->mail->Password = Mailer::PASSWORD;

        $this->mail->setFrom(Mailer::USERNAME, Mailer::NAME_FROM);

        $this->mail->addAddress($endereco, $nomeDestino);

        $this->mail->Subject = $assunto;

        $this->mail->msgHTML($html);

        $this->mail->AltBody = 'This is a plain-text message body';
    }

    public function send()
    {
        return $this->mail->send();
    }
}