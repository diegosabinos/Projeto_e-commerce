<?php

use \LS\Page;
use \LS\Model\Product;
use \LS\Model\Category;

$app->get('/', function () {

    $lista = Product::listAll();
    $produtos = Product::checkList($lista);
    $page = new Page();
    $page->setTpl("index", ['produtos' => $produtos
    ]);

});

$app->get('/categories/:idcategory', function ($idcategory) {
    $pagina = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    $category = new Category();
    $category->get((int)$idcategory);
    $paginacao = $category->getPaginaProduto($pagina);
    $paginas = [];
    for ($i = 1; $i <= $paginacao['pages']; $i++) {
        array_push($paginas, [
            'link' => '/categories/' . $category->getidcategory() . '?page=' . $i,
            'numeroPagina' => $i
        ]);
    }
    $page = new Page();

    $page->setTpl("category", [
        'category' => $category->getValues(),
        'produtos' => $paginacao['data'],
        'paginas' => $paginas
    ]);

});

$app->get('/products/:desurl', function ($desurl) {
    $produto = new Product();
    $produto->getFromURL($desurl);
    $page = new Page();

    $page->setTpl("product-detail", [
        'product' => $produto->getValues(),
        'categories' => $produto->getCategories()
    ]);
});