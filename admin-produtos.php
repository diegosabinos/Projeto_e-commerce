<?php

use \LS\PageAdmin;
use \LS\Model\User;
use \LS\Model\Product;

$app->get("/admin/products", function () {
    User::verifyLogin();

    $produtos = Product::listAll();

    $page = new PageAdmin();

    $page->setTpl("products", ["products" => $produtos]);
});

$app->get("/admin/products/create", function () {
    User::verifyLogin();

    $page = new PageAdmin();

    $page->setTpl("products-create");
});

$app->post("/admin/products/create", function () {
    User::verifyLogin();
    $produto = new Product();
    $produto->setData($_POST);
    $produto->save();
    header("Location: /admin/products");
    exit;
});

$app->get('/admin/products/:idproduto/delete', function ($idproduto) {
    User::verifyLogin();
    $produto = new Product();
    $produto->get((int)$idproduto);
    $produto->delete();
    header("Location: /admin/products");
    exit;
});


$app->get('/admin/products/:idproduto', function ($idproduto) {
    User::verifyLogin();
    $product = new Product();
    $product->get((int)$idproduto);
    $page = new PageAdmin();
    $page->setTpl("products-update", ['product' => $product->getValues()]);

});

$app->post('/admin/products/:idproduto', function ($idproduto) {
    User::verifyLogin();
    $produto = new Product();
    $produto->get((int)$idproduto);
    $produto->setData($_POST);
    $produto->save();
    $produto->setPhoto($_FILES["file"]);


    header("Location: /admin/products");
    exit;
});